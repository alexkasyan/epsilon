<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контактная информация");
?>

    <div class="row">
        <div class="col-md-5 col-lg-4">
            <div class="block contact_address">
                <div class="title">
                    <h4><?=GetMessage('CALL_US')?></h4>
                    <h5><?=GetMessage('OR_FILL')?></h5>
                </div>
                <?$APPLICATION->IncludeFile(SITE_DIR."include/contacts_page.php");?>
                <div class="block mt-4 social-media d-flex justify-content-center">
                    <a class="ct-facebookr d-flex justify-content-center align-items-center" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="ct-instagramr d-flex justify-content-center align-items-center" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-lg-8">
            <div class="contact-feedback-block">
                <?$APPLICATION->IncludeComponent(
                    "distrial:main.feedback",
                    ".default",
                    array(
                        "EMAIL_TO" => "kasyan.alexander@gmail.com",
                        "EVENT_MESSAGE_ID" => array(
                            0 => "7",
                        ),
                        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                        "REQUIRED_FIELDS" => array(
                        ),
                        "USE_CAPTCHA" => "N",
                        "COMPONENT_TEMPLATE" => ".default",
                        "EXT_FIELDS" => array(
                            0 => "Компания",
                            1 => "Телефон",
                        )
                    ),
                    false
                );?>

            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>