<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");
?>
<div class="search-page-wrap">
    <?$APPLICATION->IncludeComponent(
        "bitrix:search.page",
        "icons",
        array(
            "AJAX_MODE" => "N",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_STYLE" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "DEFAULT_SORT" => "rank",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_TEMPLATE" => "",
            "PAGER_TITLE" => "Результаты поиска",
            "PAGE_RESULT_COUNT" => "50",
            "RESTART" => "N",
            "SHOW_WHERE" => "N",
            "USE_TITLE_RANK" => "N",
            "arrFILTER" => array(
                0 => "iblock_news",
                1 => "iblock_vacancies",
                2 => "iblock_projcontent",
            ),
            "arrWHERE" => "",
            "COMPONENT_TEMPLATE" => "icons",
            "NO_WORD_LOGIC" => "N",
            "FILTER_NAME" => "",
            "arrFILTER_iblock_news" => array(
                0 => "1",
            ),
            "arrFILTER_iblock_vacancies" => array(
                0 => "2",
            ),
            "arrFILTER_iblock_service" => "",
            "arrFILTER_iblock_projcontent" => array(
                0 => "3",
                1 => "4",
                2 => "7",
            ),
            "SHOW_WHEN" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "USE_LANGUAGE_GUESS" => "Y",
            "SHOW_RATING" => "",
            "RATING_TYPE" => "",
            "PATH_TO_USER_PROFILE" => "",
            "DISPLAY_TOP_PAGER" => "Y",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "USE_SUGGEST" => "N",
            "SHOW_ITEM_TAGS" => "Y",
            "TAGS_INHERIT" => "Y",
            "SHOW_ITEM_DATE_CHANGE" => "Y",
            "SHOW_ORDER_BY" => "Y",
            "SHOW_TAGS_CLOUD" => "N",
            "STRUCTURE_FILTER" => "structure",
            "NAME_TEMPLATE" => "",
            "SHOW_LOGIN" => "Y"
        ),
        false
    );?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>