<!-- slider-area-start -->
<div class="slider-three" style="background: url(/include/banner.jpg) center center no-repeat; background-size: cover;">
    <div class="slider-area owl-carousel">
        <div class="single-slider bg-with-black">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <h3 class="intro animated">We are Very Innovitave</h3>
                            <h2 class="title animated">For all Industrial Business </h2>
                            <p class="text animated">There are many variations of passages of Lorem Ipsum available but the dummy majority have <br> suffered dumm to alteration in some form by injected dummt now.</p>
                            <div class="buttons animated">
                                <a class="link btn btn-primary" href="<?=SITE_DIR?>services/">Our Services</a>
                                <a class="link btn btn-primary activeBorder" href="<?=SITE_DIR?>contacts/">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sh-overlay"></div>
</div>
<!-- slider-area-end -->