<div class="ask_questions">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8 col-lg-6">
                <div class="home2-base-title text-center mb-2 mb-lg-5">
                    <h1>Ask Your <strong>Questions</strong></h1>
                    <p>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration by injected humour.</p>
                </div>
            </div>
        </div>
        <div class="ask_form_area">
            <?$APPLICATION->IncludeComponent(
                "distrial:main.feedback",
                "faq",
                array(
                    "EMAIL_TO" => "kasyan.alexander@gmail.com",
                    "EVENT_MESSAGE_ID" => array(
                        0 => "7",
                    ),
                    "OK_TEXT" => "Спасибо, ваш вопрос принят. В ближайшее время ответ на него появится в этом разделе.",
                    "REQUIRED_FIELDS" => array(
                    ),
                    "USE_CAPTCHA" => "N",
                    "COMPONENT_TEMPLATE" => ".default",
                    "EXT_FIELDS" => array(
                        0 => "Телефон",

                    )
                ),
                false
            );?>
        </div>
    </div>
</div>
