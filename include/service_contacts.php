<h3>Связаться с нами</h3>
<p>
	 Please feel free to contact us. We will get back to you with 1-2 business days. Or just call us now.
</p>
<p>
 <span class="font-weight-bold"><i class="fa fa-phone fw-size" aria-hidden="true"></i> CALL US:</span> +(321) 45 678 901 &amp; 902
</p>
<p class="mb-0">
 <span class="font-weight-bold"><i class="fa fa-envelope-o fw-size" aria-hidden="true"></i> MAIL US:</span> <a href="mailto:Support@Factory.com">Support@Factory.com</a>
</p>