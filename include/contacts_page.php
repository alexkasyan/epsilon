<div class="media border-bottom contact-media wow fadeInUp  animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
    <img class="mr-3" src="<?=SITE_TEMPLATE_PATH?>/img/call.png">
    <div class="media-body">
        <h5 class="mt-0 mb-0">+ (123) 1800-567-8990</h5>
        Don't hesitate to contact us!.
    </div>
</div>
<div class="media border-bottom contact-media wow fadeInUp  animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
    <img class="mr-3" src="<?=SITE_TEMPLATE_PATH?>/img/mail.png">
    <div class="media-body">
        <h5 class="mt-0 mb-0">Email Address:</h5>
        Supportteam@email.com
    </div>
</div>
<div class="media border-bottom contact-media wow fadeInUp  animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
    <img class="mr-3" src="<?=SITE_TEMPLATE_PATH?>/img/map.png">
    <div class="media-body">
        <h5 class="mt-0 mb-0">Company Headquarters</h5>
        321 Car World, 2nd Breaking Str,
        Newyork ,USA 10002.
    </div>
</div>