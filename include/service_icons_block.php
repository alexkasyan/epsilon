<div class="pt-lg-4">
    <h6 class="sub-title mb-4">Почему выбирают нас</h6>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="market-items-inner">
            <div class="card border-0 rounded-0 market-items-inner-2">
                <div class="media card-body p-0">
                    <div class="media-icon b-color">
                        <i class="flaticon-flight"></i>
                    </div>
                    <div class="media-body">
                        <h5 class="card-title mb-1">Aero Space Services</h5>
                        <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered alteration.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="market-items-inner">
            <div class="card border-0 rounded-0 market-items-inner-2">
                <div class="media card-body p-0">
                    <div class="media-icon b-color">
                        <i class="flaticon-wheel"></i>
                    </div>
                    <div class="media-body">
                        <h5 class="card-title mb-1">Lasting & Long Term</h5>
                        <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered alteration.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="market-items-inner">
            <div class="card border-0 rounded-0 market-items-inner-2">
                <div class="media card-body p-0">
                    <div class="media-icon b-color">
                        <i class="flaticon-solution"></i>
                    </div>
                    <div class="media-body">
                        <h5 class="card-title mb-1">Easy and Affortable</h5>
                        <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered alteration.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="market-items-inner">
            <div class="card border-0 rounded-0 market-items-inner-2">
                <div class="media card-body p-0">
                    <div class="media-icon b-color">
                        <i class="flaticon-solution"></i>
                    </div>
                    <div class="media-body">
                        <h5 class="card-title mb-1">Generated From Nature</h5>
                        <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered alteration.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>