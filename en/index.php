<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Epsilon");
?>

    <!-- slider-area-start -->
    <div class="slider-three" style="background-image: url(/local/templates/dustrial/img/home-3.jpg)">
        <div class="slider-area owl-carousel">
            <div class="single-slider bg-with-black">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-12">
                                <h3 class="intro animated">We are Very Innovitave</h3>
                                <h2 class="title animated">For all Industrial Business </h2>
                                <p class="text animated">There are many variations of passages of Lorem Ipsum available but the dummy majority have <br> suffered dumm to alteration in some form by injected dummt now.</p>
                                <div class="buttons animated">
                                    <a class="link btn btn-primary" href="#">Our Services</a>
                                    <a class="link btn btn-primary activeBorder" href="#">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider-area-end -->


    <!-- h3 Featured Section -->
    <div class="h3-featured">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8 col-lg-6">
                    <div class="section-title-three text-center">
                        <h1>Featured <strong>Industries</strong></h1>
                        <p>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration by injected humour.</p>
                    </div>
                </div>
            </div>

            <?$APPLICATION->IncludeComponent(
                "bitrix:news.line",
                "main_feautures_box",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "300",
                    "CACHE_TYPE" => "A",
                    "DETAIL_URL" => "/features/#ELEMENT_CODE#/",
                    "FIELD_CODE" => array(
                        0 => "CODE",
                        1 => "NAME",
                        2 => "PREVIEW_PICTURE",
                        3 => "",
                    ),
                    "IBLOCKS" => array(
                        0 => "7",
                    ),
                    "IBLOCK_TYPE" => "projcontent",
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "COMPONENT_TEMPLATE" => "main_feautures_box"
                ),
                false
            );?>

        </div>
    </div>

    <!-- h3 Market Section -->
    <div class="h3-market-section">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8 col-lg-6">
                    <div class="section-title-three text-center">
                        <h1>We Can Do <strong>Market Sectors</strong></h1>
                        <p>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration by injected humour.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 market-area">
                    <div class="market-items">

                        <!-- Market Card
                        ===================== -->
                        <div class="card border-0 rounded-0  market-style-3">
                            <div class="card-img overflowhidden position-relative">

                                <!-- Create Your Post Link here
                                ================================= -->
                                <a href="#">
                                    <div class="img-overlay activeColor d-flex align-items-center justify-content-center">
                                        <i class="fa fa-link" aria-hidden="true"></i>
                                    </div><!-- End Card Image Overlay Area -->
                                    <img class="card-img overflowhidden-top rounded-0 img-fluid" src="img/market-img-1.jpg" alt="market img 1">
                                </a><!-- End Image Link Area -->

                                <!-- You can create you tags link in heare
                                ================================================ -->
                                <a href="#">
                                    <div class="shape-style-2"></div><!-- Card Shape Style -->
                                    <div class="categories">
                                        <i class="flaticon-machine"></i>
                                    </div><!-- End Categories Area -->
                                </a>

                            </div>
                            <div class="card-body pl-0 pr-0">
                                <!-- Create Your Post Link here
                                ================================ -->
                                <a href="#">
                                    <h5 class="card-title mb-1">Construction & Engineering</h5>
                                </a>
                                <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered that alteration dummy now.</p>
                            </div>
                        </div><!-- End Card -->

                    </div><!-- End block -->
                </div><!-- End Col -->

                <div class="col-12 col-md-6 col-lg-4 market-area">
                    <div class="market-items">

                        <!-- Market Card
                        ===================== -->
                        <div class="card border-0 rounded-0  market-style-3">
                            <div class="card-img overflowhidden position-relative">

                                <!-- Create Your Post Link here
                                ================================= -->
                                <a href="#">
                                    <div class="img-overlay activeColor d-flex align-items-center justify-content-center">
                                        <i class="fa fa-link" aria-hidden="true"></i>
                                    </div><!-- End Card Image Overlay Area -->
                                    <img class="card-img overflowhidden-top rounded-0 img-fluid" src="img/market-img-2.jpg" alt="market img 2">
                                </a><!-- End Image Link Area -->

                                <!-- You can create you tags link in heare
                                ================================================ -->
                                <a href="#">
                                    <div class="shape-style-2"></div><!-- Card Shape Style -->
                                    <div class="categories">
                                        <i class="flaticon-green-energy"></i>
                                    </div><!-- End Categories Area -->
                                </a>

                            </div>
                            <div class="card-body pl-0 pr-0">
                                <!-- Create Your Post Link here
                                ================================ -->
                                <a href="#">
                                    <h5 class="card-title mb-1">Power and Energy</h5>
                                </a>
                                <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered that alteration dummy now.</p>
                            </div>
                        </div><!-- End Card -->

                    </div><!-- End block -->
                </div><!-- End Col -->

                <div class="col-12 col-md-6 col-lg-4 market-area">
                    <div class="market-items">

                        <!-- Market Card
                        ===================== -->
                        <div class="card border-0 rounded-0  market-style-3">
                            <div class="card-img overflowhidden position-relative">

                                <!-- Create Your Post Link here
                                ================================= -->
                                <a href="#">
                                    <div class="img-overlay activeColor d-flex align-items-center justify-content-center">
                                        <i class="fa fa-link" aria-hidden="true"></i>
                                    </div><!-- End Card Image Overlay Area -->
                                    <img class="card-img overflowhidden-top rounded-0 img-fluid" src="img/market-img-3.jpg" alt="market img 3">
                                </a><!-- End Image Link Area -->

                                <!-- You can create you tags link in heare
                                ================================================ -->
                                <a href="#">
                                    <div class="shape-style-2"></div><!-- Card Shape Style -->
                                    <div class="categories">
                                        <i class="flaticon-ship"></i>
                                    </div><!-- End Categories Area -->
                                </a>

                            </div>
                            <div class="card-body pl-0 pr-0">
                                <!-- Create Your Post Link here
                                ================================ -->
                                <a href="#">
                                    <h5 class="card-title mb-1">Ship Building Industry</h5>
                                </a>
                                <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered that alteration dummy now.</p>
                            </div>
                        </div><!-- End Card -->

                    </div><!-- End block -->
                </div><!-- End Col -->

                <div class="col-12 col-md-6 col-lg-4 market-area">
                    <div class="market-items">

                        <!-- Market Card
                        ===================== -->
                        <div class="card border-0 rounded-0  market-style-3">
                            <div class="card-img overflowhidden position-relative">

                                <!-- Create Your Post Link here
                                ================================= -->
                                <a href="#">
                                    <div class="img-overlay activeColor d-flex align-items-center justify-content-center">
                                        <i class="fa fa-link" aria-hidden="true"></i>
                                    </div><!-- End Card Image Overlay Area -->
                                    <img class="card-img overflowhidden-top rounded-0 img-fluid" src="img/market-img-4.jpg" alt="market img 4">
                                </a><!-- End Image Link Area -->

                                <!-- You can create you tags link in heare
                                ================================================ -->
                                <a href="#">
                                    <div class="shape-style-2"></div><!-- Card Shape Style -->
                                    <div class="categories">
                                        <i class="flaticon-flight"></i>
                                    </div><!-- End Categories Area -->
                                </a>

                            </div>
                            <div class="card-body pl-0 pr-0">
                                <!-- Create Your Post Link here
                                ================================ -->
                                <a href="#">
                                    <h5 class="card-title mb-1">Aero Space Services</h5>
                                </a>
                                <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered that alteration dummy now.</p>
                            </div>
                        </div><!-- End Card -->

                    </div><!-- End block -->
                </div><!-- End Col -->

                <div class="col-12 col-md-6 col-lg-4 market-area">
                    <div class="market-items">

                        <!-- Market Card
                        ===================== -->
                        <div class="card border-0 rounded-0  market-style-3">
                            <div class="card-img overflowhidden position-relative">

                                <!-- Create Your Post Link here
                                ================================= -->
                                <a href="#">
                                    <div class="img-overlay activeColor d-flex align-items-center justify-content-center">
                                        <i class="fa fa-link" aria-hidden="true"></i>
                                    </div><!-- End Card Image Overlay Area -->
                                    <img class="card-img overflowhidden-top rounded-0 img-fluid" src="img/market-img-5.jpg" alt="market img 5">
                                </a><!-- End Image Link Area -->

                                <!-- You can create you tags link in heare
                                ================================================ -->
                                <a href="#">
                                    <div class="shape-style-2"></div><!-- Card Shape Style -->
                                    <div class="categories">
                                        <i class="flaticon-gear"></i>
                                    </div><!-- End Categories Area -->
                                </a>

                            </div>
                            <div class="card-body pl-0 pr-0">
                                <!-- Create Your Post Link here
                                ================================ -->
                                <a href="#">
                                    <h5 class="card-title mb-1">Maintenance</h5>
                                </a>
                                <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered that alteration dummy now.</p>
                            </div>
                        </div><!-- End Card -->

                    </div><!-- End block -->
                </div><!-- End Col -->

                <div class="col-12 col-md-6 col-lg-4 market-area">
                    <div class="market-items">

                        <!-- Market Card
                        ===================== -->
                        <div class="card border-0 rounded-0  market-style-3">
                            <div class="card-img overflowhidden position-relative">

                                <!-- Create Your Post Link here
                                ================================= -->
                                <a href="#">
                                    <div class="img-overlay activeColor d-flex align-items-center justify-content-center">
                                        <i class="fa fa-link" aria-hidden="true"></i>
                                    </div><!-- End Card Image Overlay Area -->
                                    <img class="card-img overflowhidden-top rounded-0 img-fluid" src="img/market-img-6.jpg" alt="market img 6">
                                </a><!-- End Image Link Area -->

                                <!-- You can create you tags link in heare
                                ================================================ -->
                                <a href="#">
                                    <div class="shape-style-2"></div><!-- Card Shape Style -->
                                    <div class="categories">
                                        <i class="flaticon-motor"></i>
                                    </div><!-- End Categories Area -->
                                </a>

                            </div>
                            <div class="card-body pl-0 pr-0">
                                <!-- Create Your Post Link here
                                ================================ -->
                                <a href="#">
                                    <h5 class="card-title mb-1">Ship Building Industry</h5>
                                </a>
                                <p class="card-text">There are many variations of passages the a Lorem Ipsum available but the majority the suffered that alteration dummy now.</p>
                            </div>
                        </div><!-- End Card -->

                    </div><!-- End block -->
                </div><!-- End Col -->

            </div>
        </div>
    </div>
    <!-- End h3 Market Section -->

    <!-- h3 Latest Projects -->
    <div class="h3-latest-project">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-8 col-lg-6">
                            <div class="section-title-three text-center">
                                <h1 class="text-light">Latest <strong>Projects</strong></h1>
                                <p class="text-light">There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration by injected humour.</p>
                            </div>
                        </div>
                    </div>

                    <!-- Letest Project Btn -->
                    <div class="h3-mixitup-menus">
                        <button class="btn btn-primary" type="button" data-mixitup-control data-filter="all">View All</button>
                        <button class="btn btn-primary" type="button" data-mixitup-control data-filter=".agriculture">Agriculture</button>
                        <button class="btn btn-primary" type="button" data-mixitup-control data-filter=".chemical">Chemical</button>
                        <button class="btn btn-primary" type="button" data-mixitup-control data-filter=".materials">Materials</button>
                        <button class="btn btn-primary" type="button" data-mixitup-control data-filter=".mechanical">Mechanical</button>
                        <button class="btn btn-primary" type="button" data-mixitup-control data-filter=".powerEnergy">Power & Energy</button>
                    </div><!-- End Project Btn -->

                    <!-- Letest Project content -->
                    <div id="mixitup-projects" class="row">
                        <div class="mix agriculture chemical col-12 col-md-6 col-lg-4">
                            <div class="card border-0 rounded-0">
                                <div class="card-img position-relative">

                                    <!-- Create Your Post Link here
                                    ================================= -->
                                    <a href="#">
                                        <div class="img-overlay style-1 activeColor d-flex align-items-center justify-content-center text-center">
                                            <div class="col-md-10">
                                                <h5 class="text-light">Industy</h5>
                                                <p class="text-light">There are many variations of passages a of Lorem Ipsum available but the at majority suffered.</p>
                                            </div>
                                        </div><!-- End Card Image Overlay Area -->
                                        <img class="card-img-top rounded-0 img-fluid" src="img/mix-img-1.jpg" alt="market img 1">
                                    </a><!-- End Image Link Area -->

                                    <!-- You can create you tags link in heare
                                    ================================================ -->
                                    <a href="#">
                                        <div class="shape"></div><!-- Card Shape Style -->
                                        <div class="categories">
                                            <img class="img-fluid" src="img/big-arrow.png" alt="cat img">
                                        </div><!-- End Categories Area -->
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="mix chemical agriculture materials col-12 col-md-6 col-lg-4">
                            <div class="card border-0 rounded-0">
                                <div class="card-img position-relative">

                                    <!-- Create Your Post Link here
                                    ================================= -->
                                    <a href="#">
                                        <div class="img-overlay style-1 activeColor d-flex align-items-center justify-content-center text-center">
                                            <div class="col-md-10">
                                                <h5 class="text-light">Industy</h5>
                                                <p class="text-light">There are many variations of passages a of Lorem Ipsum available but the at majority suffered.</p>
                                            </div>
                                        </div><!-- End Card Image Overlay Area -->
                                        <img class="card-img-top rounded-0 img-fluid" src="img/mix-img-2.jpg" alt="market img 1">
                                    </a><!-- End Image Link Area -->

                                    <!-- You can create you tags link in heare
                                    ================================================ -->
                                    <a href="#">
                                        <div class="shape"></div><!-- Card Shape Style -->
                                        <div class="categories">
                                            <img class="img-fluid" src="img/big-arrow.png" alt="cat img">
                                        </div><!-- End Categories Area -->
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="mix materials chemical mechanical col-12 col-md-6 col-lg-4">
                            <div class="card border-0 rounded-0">
                                <div class="card-img position-relative">

                                    <!-- Create Your Post Link here
                                    ================================= -->
                                    <a href="#">
                                        <div class="img-overlay style-1 activeColor d-flex align-items-center justify-content-center text-center">
                                            <div class="col-md-10">
                                                <h5 class="text-light">Industy</h5>
                                                <p class="text-light">There are many variations of passages a of Lorem Ipsum available but the at majority suffered.</p>
                                            </div>
                                        </div><!-- End Card Image Overlay Area -->
                                        <img class="card-img-top rounded-0 img-fluid" src="img/mix-img-3.jpg" alt="market img 1">
                                    </a><!-- End Image Link Area -->

                                    <!-- You can create you tags link in heare
                                    ================================================ -->
                                    <a href="#">
                                        <div class="shape"></div><!-- Card Shape Style -->
                                        <div class="categories">
                                            <img class="img-fluid" src="img/big-arrow.png" alt="cat img">
                                        </div><!-- End Categories Area -->
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="mix mechanical materials powerEnergy agriculture col-12 col-md-6 col-lg-4">
                            <div class="card border-0 rounded-0">
                                <div class="card-img position-relative">

                                    <!-- Create Your Post Link here
                                    ================================= -->
                                    <a href="#">
                                        <div class="img-overlay style-1 activeColor d-flex align-items-center justify-content-center text-center">
                                            <div class="col-md-10">
                                                <h5 class="text-light">Industy</h5>
                                                <p class="text-light">There are many variations of passages a of Lorem Ipsum available but the at majority suffered.</p>
                                            </div>
                                        </div><!-- End Card Image Overlay Area -->
                                        <img class="card-img-top rounded-0 img-fluid" src="img/mix-img-4.jpg" alt="market img 1">
                                    </a><!-- End Image Link Area -->

                                    <!-- You can create you tags link in heare
                                    ================================================ -->
                                    <a href="#">
                                        <div class="shape"></div><!-- Card Shape Style -->
                                        <div class="categories">
                                            <img class="img-fluid" src="img/big-arrow.png" alt="cat img">
                                        </div><!-- End Categories Area -->
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="mix powerEnergy mechanical chemical agriculture col-12 col-md-6 col-lg-4">
                            <div class="card border-0 rounded-0">
                                <div class="card-img position-relative">

                                    <!-- Create Your Post Link here
                                    ================================= -->
                                    <a href="#">
                                        <div class="img-overlay style-1 activeColor d-flex align-items-center justify-content-center text-center">
                                            <div class="col-md-10">
                                                <h5 class="text-light">Industy</h5>
                                                <p class="text-light">There are many variations of passages a of Lorem Ipsum available but the at majority suffered.</p>
                                            </div>
                                        </div><!-- End Card Image Overlay Area -->
                                        <img class="card-img-top rounded-0 img-fluid" src="img/mix-img-5.jpg" alt="market img 1">
                                    </a><!-- End Image Link Area -->

                                    <!-- You can create you tags link in heare
                                    ================================================ -->
                                    <a href="#">
                                        <div class="shape"></div><!-- Card Shape Style -->
                                        <div class="categories">
                                            <img class="img-fluid" src="img/big-arrow.png" alt="cat img">
                                        </div><!-- End Categories Area -->
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="mix powerEnergy agriculture chemical col-12 col-md-6 col-lg-4">
                            <div class="card border-0 rounded-0">
                                <div class="card-img position-relative">

                                    <!-- Create Your Post Link here
                                    ================================= -->
                                    <a href="#">
                                        <div class="img-overlay style-1 activeColor d-flex align-items-center justify-content-center text-center">
                                            <div class="col-md-10">
                                                <h5 class="text-light">Industy</h5>
                                                <p class="text-light">There are many variations of passages a of Lorem Ipsum available but the at majority suffered.</p>
                                            </div>
                                        </div><!-- End Card Image Overlay Area -->
                                        <img class="card-img-top rounded-0 img-fluid" src="img/mix-img-6.jpg" alt="market img 1">
                                    </a><!-- End Image Link Area -->

                                    <!-- You can create you tags link in heare
                                    ================================================ -->
                                    <a href="#">
                                        <div class="shape"></div><!-- Card Shape Style -->
                                        <div class="categories">
                                            <img class="img-fluid" src="img/big-arrow.png" alt="cat img">
                                        </div><!-- End Categories Area -->
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End h3 Letest Projects -->

    <!-- h3 Management Team -->
    <div class="h3-management">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8 col-lg-6">
                    <div class="section-title-three text-center">
                        <h1>Our <strong>Management</strong></h1>
                        <p>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration by injected humour.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="team-item col-12 col-md-6 col-lg-3">
                    <div class="card rounded-0 border-0">
                        <div class="team-thumb position-relative">
                            <img class="card-img-top" src="img/team-01.jpg" alt="Team Image">
                            <div class="team-social d-flex">
                                <a href="#" class="ct-facebook d-flex justify-content-center align-items-center"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-twitter d-flex justify-content-center align-items-center"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-linkedin d-flex justify-content-center align-items-center"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-skype d-flex justify-content-center align-items-center"> <i class="fa fa-skype" aria-hidden="true"></i> </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title mb-0">Robert Pattinson</h5>
                            <p class="card-text">Engineering Officer</p>
                        </div>
                    </div>
                </div>

                <div class="team-item col-12 col-md-6 col-lg-3">
                    <div class="card rounded-0 border-0">
                        <div class="team-thumb position-relative">
                            <img class="card-img-top" src="img/team-02.jpg" alt="Team Image">
                            <div class="team-social d-flex">
                                <a href="#" class="ct-facebook d-flex justify-content-center align-items-center"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-twitter d-flex justify-content-center align-items-center"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-linkedin d-flex justify-content-center align-items-center"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-skype d-flex justify-content-center align-items-center"> <i class="fa fa-skype" aria-hidden="true"></i> </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title mb-0">David Mazouz</h5>
                            <p class="card-text">Marketing Manager</p>
                        </div>
                    </div>
                </div>

                <div class="team-item col-12 col-md-6 col-lg-3">
                    <div class="card rounded-0 border-0">
                        <div class="team-thumb position-relative">
                            <img class="card-img-top" src="img/team-03.jpg" alt="Team Image">
                            <div class="team-social d-flex">
                                <a href="#" class="ct-facebook d-flex justify-content-center align-items-center"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-twitter d-flex justify-content-center align-items-center"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-linkedin d-flex justify-content-center align-items-center"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-skype d-flex justify-content-center align-items-center"> <i class="fa fa-skype" aria-hidden="true"></i> </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title mb-0">Moira Kelly</h5>
                            <p class="card-text">Technology Officer</p>
                        </div>
                    </div>
                </div>

                <div class="team-item col-12 col-md-6 col-lg-3">
                    <div class="card rounded-0 border-0">
                        <div class="team-thumb position-relative">
                            <img class="card-img-top" src="img/team-04.jpg" alt="Team Image">
                            <div class="team-social d-flex">
                                <a href="#" class="ct-facebook d-flex justify-content-center align-items-center"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-twitter d-flex justify-content-center align-items-center"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-linkedin d-flex justify-content-center align-items-center"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                <a href="#" class="ct-skype d-flex justify-content-center align-items-center"> <i class="fa fa-skype" aria-hidden="true"></i> </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title mb-0">Cristian Pavon</h5>
                            <p class="card-text">Technology Officer</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- h3 Blog Section -->
    <div class="h2-blog">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8 col-lg-6">
                    <div class="section-title-three text-center">
                        <h1>Последние <strong>новости</strong></h1>
                        <p>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration by injected humour.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <div id="blog-list-3" class="owl-carousel">

                            <div class="h2-blog-single-item">
                                <div class="blog-img">
                                    <img class="img-fluid" src="img/blog-big-2.png" alt="blog img">
                                    <div class="overlay-btn">
                                        <a href="#" class="btn btn-primary">Engineering</a>
                                    </div>
                                </div>
                                <div class="card rounded-0">
                                    <div class="card-body">
                                        <div class="article-content">
                                            <div class="entry-meta">
                                                <div class="author">By <a href="#">Clifford Donley</a></div>
                                                <div class="month"><a href="#">May 1, 2018</a></div>
                                            </div>
                                            <a href="#" class="entry-title">Where the only way of saving the world will be for industrial civilization</a>
                                            <p class="entry-content">There are many variations of passages of Lorem Ipsum available but the abo  majority have suffered alteration in that  some form by injected humour the  randomised it words which don't look even slightly believable. If you are it going a to use a dumm  passage.</p>
                                        </div>
                                        <div class="entry-meta-footer">
                                            <a href="#" class="read-more">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> Comment: 15</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="h2-blog-single-item">
                                <div class="blog-img">
                                    <img class="img-fluid" src="img/blog-big-3.png" alt="blog img">
                                    <div class="overlay-btn">
                                        <a href="#" class="btn btn-primary">Mechanical</a>
                                    </div>
                                </div>
                                <div class="card rounded-0">
                                    <div class="card-body">
                                        <div class="article-content">
                                            <div class="entry-meta">
                                                <div class="author">
                                                    By <a href="#">Rebecca Garza</a>
                                                </div>
                                                <div class="month">
                                                    <a href="#">June 17, 2018</a>
                                                </div>
                                            </div>
                                            <a href="#" class="entry-title">We are the world class engineering that providing quality products</a>
                                            <p class="entry-content">There are many variations of passages of Lorem Ipsum available but the abo  majority have suffered alteration in that  some form by injected humour the  randomised it words which don't look even slightly believable. If you are it going a to use a dumm  passage.</p>
                                        </div>
                                        <div class="entry-meta-footer">
                                            <a href="#" class="read-more">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> Comment: 22</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="h2-blog-single-item">
                                <div class="blog-img">
                                    <img class="img-fluid" src="img/blog-big-2.png" alt="blog img">
                                    <div class="overlay-btn">
                                        <a href="#" class="btn btn-primary">Engineering</a>
                                    </div>
                                </div>
                                <div class="card rounded-0">
                                    <div class="card-body">
                                        <div class="article-content">
                                            <div class="entry-meta">
                                                <div class="author">
                                                    By <a href="#">Rebecca Garza</a>
                                                </div>
                                                <div class="month">
                                                    <a href="#">June 17, 2018</a>
                                                </div>
                                            </div>
                                            <a href="#" class="entry-title">We are the world class engineering that providing quality products</a>
                                            <p class="entry-content">There are many variations of passages of Lorem Ipsum available but the abo  majority have suffered alteration in that  some form by injected humour the  randomised it words which don't look even slightly believable. If you are it going a to use a dumm  passage.</p>
                                        </div>
                                        <div class="entry-meta-footer">
                                            <a href="#" class="read-more">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> Comment: 22</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="h2-blog-single-item">
                                <div class="blog-img">
                                    <img class="img-fluid" src="img/blog-big-3.png" alt="blog img">
                                    <div class="overlay-btn">
                                        <a href="#" class="btn btn-primary">Engineering</a>
                                    </div>
                                </div>
                                <div class="card rounded-0">
                                    <div class="card-body">
                                        <div class="article-content">
                                            <div class="entry-meta">
                                                <div class="author">
                                                    By <a href="#">Rebecca Garza</a>
                                                </div>
                                                <div class="month">
                                                    <a href="#">June 17, 2018</a>
                                                </div>
                                            </div>
                                            <a href="#" class="entry-title">We are the world class engineering that manufacturer quality products</a>
                                            <p class="entry-content">There are many variations of passages of Lorem Ipsum available but the abo  majority have suffered alteration in that  some form by injected humour the  randomised it words which don't look even slightly believable. If you are it going a to use a dumm  passage.</p>
                                        </div>
                                        <div class="entry-meta-footer">
                                            <a href="#" class="read-more">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> Comment: 22</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="h2-blog-single-item">
                                <div class="blog-img">
                                    <img class="img-fluid" src="img/blog-big-2.png" alt="blog img">
                                    <div class="overlay-btn">
                                        <a href="#" class="btn btn-primary">Engineering</a>
                                    </div>
                                </div>
                                <div class="card rounded-0">
                                    <div class="card-body">
                                        <div class="article-content">
                                            <div class="entry-meta">
                                                <div class="author">
                                                    By <a href="#">Clifford Donley</a>
                                                </div>
                                                <div class="month">
                                                    <a href="#">May 1, 2018</a>
                                                </div>
                                            </div>
                                            <a href="#" class="entry-title">Where the only way of saving the world will be for industrial civilization</a>
                                            <p class="entry-content">There are many variations of passages of Lorem Ipsum available but the abo  majority have suffered alteration in that  some form by injected humour the  randomised it words which don't look even slightly believable. If you are it going a to use a dumm  passage.</p>
                                        </div>
                                        <div class="entry-meta-footer">
                                            <a href="#" class="read-more">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> Comment: 15</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="h2-blog-single-item">
                                <div class="blog-img">
                                    <img class="img-fluid" src="img/blog-big-3.png" alt="blog img">
                                    <div class="overlay-btn">
                                        <a href="#" class="btn btn-primary">Mechanical</a>
                                    </div>
                                </div>
                                <div class="card rounded-0">
                                    <div class="card-body">
                                        <div class="article-content">
                                            <div class="entry-meta">
                                                <div class="author">
                                                    By <a href="#">Rebecca Garza</a>
                                                </div>
                                                <div class="month">
                                                    <a href="#">June 17, 2018</a>
                                                </div>
                                            </div>
                                            <a href="#" class="entry-title">We are the world class engineering that manufacturer quality products</a>
                                            <p class="entry-content">There are many variations of passages of Lorem Ipsum available but the abo  majority have suffered alteration in that  some form by injected humour the  randomised it words which don't look even slightly believable. If you are it going a to use a dumm  passage.</p>
                                        </div>
                                        <div class="entry-meta-footer">
                                            <a href="#" class="read-more">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> Comment: 22</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="h2-blog-single-item">
                                <div class="blog-img">
                                    <img class="img-fluid" src="img/blog-big-2.png" alt="blog img">
                                    <div class="overlay-btn">
                                        <a href="#" class="btn btn-primary">Engineering</a>
                                    </div>
                                </div>
                                <div class="card rounded-0">
                                    <div class="card-body">
                                        <div class="article-content">
                                            <div class="entry-meta">
                                                <div class="author">
                                                    By <a href="#">Rebecca Garza</a>
                                                </div>
                                                <div class="month">
                                                    <a href="#">June 17, 2018</a>
                                                </div>
                                            </div>
                                            <a href="#" class="entry-title">We are the world class engineering that manufacturer quality products</a>
                                            <p class="entry-content">There are many variations of passages of Lorem Ipsum available but the abo  majority have suffered alteration in that  some form by injected humour the  randomised it words which don't look even slightly believable. If you are it going a to use a dumm  passage.</p>
                                        </div>
                                        <div class="entry-meta-footer">
                                            <a href="#" class="read-more">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> Comment: 22</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="h2-blog-single-item">
                                <div class="blog-img">
                                    <img class="img-fluid" src="img/blog-big-3.png" alt="blog img">
                                    <div class="overlay-btn">
                                        <a href="#" class="btn btn-primary">Engineering</a>
                                    </div>
                                </div>
                                <div class="card rounded-0">
                                    <div class="card-body">
                                        <div class="article-content">
                                            <div class="entry-meta">
                                                <div class="author">
                                                    By <a href="#">Rebecca Garza</a>
                                                </div>
                                                <div class="month">
                                                    <a href="#">June 17, 2018</a>
                                                </div>
                                            </div>
                                            <a href="#" class="entry-title">We are the world class engineering that manufacturer quality products</a>
                                            <p class="entry-content">There are many variations of passages of Lorem Ipsum available but the abo  majority have suffered alteration in that  some form by injected humour the  randomised it words which don't look even slightly believable. If you are it going a to use a dumm  passage.</p>
                                        </div>
                                        <div class="entry-meta-footer">
                                            <a href="#" class="read-more">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> Comment: 22</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End h3 Blog Section -->

    <!-- Sponsored Section -->
    <div class="sponsored">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="client-carousel-items" class="owl-carousel">
                        <div class="client-single-items">
                            <img class="img-fluid sponsored-img" src="img/sponsored-img-1.png" alt="sponsored-img">
                        </div>
                        <div class="client-single-items">
                            <img class="img-fluid sponsored-img" src="img/sponsored-img-2.png" alt="sponsored-img">
                        </div>
                        <div class="client-single-items">
                            <img class="img-fluid sponsored-img" src="img/sponsored-img-3.png" alt="sponsored-img">
                        </div>
                        <div class="client-single-items">
                            <img class="img-fluid sponsored-img" src="img/sponsored-img-4.png" alt="sponsored-img">
                        </div>
                        <div class="client-single-items">
                            <img class="img-fluid sponsored-img" src="img/sponsored-img-5.png" alt="sponsored-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Sponsored Section -->

<?/*$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	".default",
	Array(
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "1",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DISPLAY_PANEL" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
		"PAGER_SHOW_ALL" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);*/?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>