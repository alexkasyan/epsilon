<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");


/*$APPLICATION->IncludeComponent("bitrix:main.map", ".default", Array(
	"LEVEL"	=>	"3",
	"COL_NUM"	=>	"2",
	"SHOW_DESCRIPTION"	=>	"Y",
	"SET_TITLE"	=>	"Y",
	"CACHE_TIME"	=>	"36000000"
	)
);*/
?>
    <!-- 404 Content -->
    <div class="error_404">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="error_content text-center">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/404.png" class="img-fluid" alt="">
                        <!--<p>SORRY! THE PAGE YOU WERE LOOKING FOR, COULDN'T BE FOUND.</p>
                        <a href="/" class="btn btn-primary">BACK TO HOME PAGE</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End 404 Content -->
<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>