<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<ul>
    <?php foreach ($arResult['SECTIONS'] as $SECTION) {?>
        <? if ($SECTION['SECTION_PAGE_URL'] == $APPLICATION->GetCurDir()){
            $class = 'selected';
        }?>
        <li class="d-flex justify-content-between news-cat-link" class="<?=$class?>">
            <a href="<?=$SECTION['SECTION_PAGE_URL']?>" class="m-0"><?=$SECTION['NAME']?>  </a>
            <a href="<?=$SECTION['SECTION_PAGE_URL']?>" class="m-0">(<?=$SECTION['ELEMENT_CNT']?>)</a>
        </li>
    <?php }?>
</ul>
