<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="d-flex justify-content-center pagination_waper project_column_3">
            <nav aria-label="Page navigation ct-pagination">
                <ul class="pagination">
    <?

    $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
    $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
    ?>

                    <?
    if($arResult["bDescPageNumbering"] === true):
        $bFirst = true;
        if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
            if($arResult["bSavePage"]):
                ?>

                <li class="page-item">
                    <a class=" page-link pt-0 pb-0 rounded-0  activeColor" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
                        <span aria-hidden="true">«</span>
                        <span class="sr-only"><?=GetMessage("nav_prev")?></span>
                        </a></li>
                <?
            else:
                if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):
                    ?>
                    <li class="page-item"> <a class="page-link pt-0 pb-0 rounded-0 activeColor" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">
                            <span aria-hidden="true">«</span>
                            <span class="sr-only"><?=GetMessage("nav_prev")?></span>
                            </a>
                    </li>
                    <?
                else:
                    ?>
                    <li class="page-item"><a class="page-link pt-0 pb-0 rounded-0 activeColor"
                                             href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">

                            <span aria-hidden="true">«</span>
                            <span class="sr-only"><?=GetMessage("nav_prev")?></span>
                        </a>
                    </li>
                    <?
                endif;
            endif;

            if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
                $bFirst = false;
                if($arResult["bSavePage"]):
                    ?>
                    <li class="page-item"><a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>">1</a></li>
                    <?
                else:
                    ?>
                    <li class="page-item"> <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a></li>
                    <?
                endif;
                if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):
                    /*?>
                                <span class="modern-page-dots">...</span>
                    <?*/
                    ?>
                    <li class="page-item"><a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=intVal($arResult["nStartPage"] + ($arResult["NavPageCount"] - $arResult["nStartPage"]) / 2)?>">...</a></li>
                    <?
                endif;
            endif;
        endif;
        do
        {
            $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

            if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                ?>
                <li class="page-item">
                    <a href="" class="page-link pt-0 pb-0 rounded-0 text-white activeBg">
                        <?=$NavRecordGroupPrint?>
                    </a>

                </li>
                <?
            elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
                ?>
                <li class="page-item">
                    <a
                            class="page-link pt-0 pb-0 rounded-0 text-dark"
                            href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$NavRecordGroupPrint?></a></li>
                <?
            else:
                ?>
                    <li class="page-item"> <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"<?
                        ?> ><?=$NavRecordGroupPrint?></a></li>
                <?
            endif;

            $arResult["nStartPage"]--;
            $bFirst = false;
        } while($arResult["nStartPage"] >= $arResult["nEndPage"]);

        if ($arResult["NavPageNomer"] > 1):
            if ($arResult["nEndPage"] > 1):
                if ($arResult["nEndPage"] > 2):
                    /*?>
                            <span class="modern-page-dots">...</span>
                    <?*/
                    ?>
                    <li class="page-item"><a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=round($arResult["nEndPage"] / 2)?>">...</a></li>
                    <?
                endif;
                ?>
                <li class="page-item"> <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=$arResult["NavPageCount"]?></a></li>
                <?
            endif;

            ?>
            <li class="page-item"><a class="page-link pt-0 pb-0 rounded-0 activeColor"
                                     href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">
                    <span aria-hidden="true">»</span>
                    <span class="sr-only"><?=GetMessage("nav_next")?></span>
                </a></li>
            <?
        endif;

    else:
        $bFirst = true;

        if ($arResult["NavPageNomer"] > 1):
            if($arResult["bSavePage"]):
                ?>
                <li class="page-item">  <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">

                        <span aria-hidden="true">«</span>
                        <span class="sr-only"><?=GetMessage("nav_prev")?></span>
                    </a>
                </li>
                <?
            else:
                if ($arResult["NavPageNomer"] > 2):
                    ?>
                    <li class="page-item">  <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">
                            <span aria-hidden="true">«</span>
                            <span class="sr-only"><?=GetMessage("nav_prev")?></span>
                            </a></li>
                    <?
                else:
                    ?>
                    <li class="page-item">
                        <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">
                            <span aria-hidden="true">«</span>
                            <span class="sr-only"><?=GetMessage("nav_prev")?></span>
                        </a>
                    </li>
                    <?
                endif;

            endif;

            if ($arResult["nStartPage"] > 1):
                $bFirst = false;
                if($arResult["bSavePage"]):
                    ?>
                    <li class="page-item">  <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a></li>
                    <?
                else:
                    ?>
                    <li class="page-item">   <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a></li>
                    <?
                endif;
                if ($arResult["nStartPage"] > 2):
                    /*?>
                                <span class="modern-page-dots">...</span>
                    <?*/
                    ?>
                    <li class="page-item">  <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=round($arResult["nStartPage"] / 2)?>">...</a></li>
                    <?
                endif;
            endif;
        endif;

        do
        {
            if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                ?>
                <li class="page-item">
                    <a class="page-link pt-0 pb-0 rounded-0 text-white activeBg" href="#"><?=$arResult["nStartPage"]?></a>

                </li>
                <?
            elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
                ?>
                <li class="page-item">
                    <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="page-link pt-0 pb-0 rounded-0 text-dark">
                        <?=$arResult["nStartPage"]?>
                    </a>
                </li>
                <?
            else:
                ?>
                    <li class="page-item">
                        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"<?
                        ?> class="page-link pt-0 pb-0 rounded-0 text-dark"><?=$arResult["nStartPage"]?></a></li>
                <?
            endif;
            $arResult["nStartPage"]++;
            $bFirst = false;
        } while($arResult["nStartPage"] <= $arResult["nEndPage"]);

        if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
            if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
                if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):
                    /*?>
                            <span class="modern-page-dots">...</span>
                    <?*/
                    ?>
                    <li class="page-item">   <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=round($arResult["nEndPage"] + ($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2)?>">...</a></li>
                    <?
                endif;
                ?>
                <li class="page-item">  <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a></li>
                <?
            endif;
            ?>
            <li class="page-item">  <a class="page-link pt-0 pb-0 rounded-0 text-dark"
                                       href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">

                    <span aria-hidden="true">»</span>
                    <span class="sr-only"><?=GetMessage("nav_next")?></span>
                    </a></li>
            <?
        endif;
    endif;

    if ($arResult["bShowAll"]):
        if ($arResult["NavShowAll"]):
            ?>
                    <li class="page-item">
                        <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0"><?=GetMessage("nav_paged")?></a></li>
            <?
        else:
            ?>
                    <li class="page-item">
                        <a class="page-link pt-0 pb-0 rounded-0 text-dark" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1"><?=GetMessage("nav_all")?></a> </li>
            <?
        endif;
    endif
    ?>
                </ul>
            </nav>
        </div>
    </div>
</div>