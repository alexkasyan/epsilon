<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block">
    <h5 class="footer-nav-title text-light"><?=GetMessage('LATEST_NEWS')?></h5>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="media footer-block" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <img class="mr-3" src="<?=$arItem['PREVIEWS_PICTURE']['SRC']?>">
            <div class="media-body">
                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                    <h6 class="m-0 activeColor footer-blog-date"><?=$arItem['DISPLAY_ACTIVE_FROM']?></h6>
                    <p class="m-0 footer-blog-title"><?=$arItem['NAME']?></p>
                </a>
            </div>
        </div>
    <?endforeach;?>
</div>