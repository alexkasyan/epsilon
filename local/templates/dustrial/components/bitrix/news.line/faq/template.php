<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$cnt = round(count($arResult["ITEMS"]) / 2);
$arTmp = array_chunk($arResult["ITEMS"], $cnt);
foreach ($arTmp as $k => $items) { ?>

    <div class="<? if ($k == 0) {
        echo 'col-md-12 col-lg-6 mb-5 mb-lg-0';
    } else {
        echo 'col-md-12 col-lg-6';
    } ?>">
        <div id="<? if ($k == 0) {
            echo 'faq-page-content';
        } else {
            echo 'faq-page-content-two';
        } ?>">
            <div class="accordion" id="<? if ($k == 0) {
                echo 'accordionExample';
            } else {
                echo 'accordionExampletwo';
            } ?>">
                <? foreach ($items as $item) { ?>
                    <div class="card <? if ($item !== end($items)) { ?>mb-3<? } ?>">
                        <a class="card-header collapsed" role="button" data-toggle="collapse"
                           data-target="#collapse<?= $item['ID'] ?>" aria-expanded="false" aria-controls="collapseFour">
                            <div class="card-title">
                                <? echo $item["NAME"] ?>
                            </div>
                        </a>
                        <div id="collapse<?= $item['ID'] ?>" class="collapse" data-parent="#<? if ($k == 0) {
                            echo 'accordionExample';
                        } else {
                            echo 'accordionExampletwo';
                        } ?>" style="">
                            <div class="card-body">
                                <? echo $item["PREVIEW_TEXT"] ?>
                            </div>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>
    </div>

<? } ?>

