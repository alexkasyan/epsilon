<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="col-md-12 col-lg-7 col-xl-8 order-0 order-lg-1">
    <div class="market-content-page">
        <div class="block">
            <div class="text-img-block">
                <img class="img-fluid" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="">
                <h3 class="services-right-title"><?=$arResult['NAME']?></h3>
                <div class="detail-text">
                    <?=$arResult['DETAIL_TEXT']?>
                </div>
            </div>
            <?$APPLICATION->IncludeFile(SITE_DIR."include/service_icons_block.php");?>
            <!--<div class="row border-top pt-lg-4">
                <div class="col-md-5 col-lg-12 col-xl-6 pl-lg-0">
                    <img class="img-fluid" src="img/service-bottom.png" alt="">
                </div>
                <div class="col-md-7 col-lg-12 col-xl-6 mt-3 mt-md-0 mt-lg-3 mt-xl-0 pl-lg-0">
                    <div>
                        <h6 class="sub-title mb-1">Research & Development</h6>
                    </div>
                    <p>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration in at that  some form by injected.</p>
                    <div class="list_content">
                        <p class="mb-2"><i class="fa fa-check mr-1 mr-lg-3 activeColor" aria-hidden="true"></i> There are many variations of passages dummy it.</p>
                        <p class="mb-2"><i class="fa fa-check mr-1 mr-lg-3 activeColor" aria-hidden="true"></i> Many variations of passages dummy it.</p>
                        <p class="mb-2"><i class="fa fa-check mr-1 mr-lg-3 activeColor" aria-hidden="true"></i> The majority have suffered alteration in that.</p>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</div>
