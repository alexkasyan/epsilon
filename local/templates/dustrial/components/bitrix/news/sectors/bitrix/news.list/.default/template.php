<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- Market Section -->
<div class="section-space">
    <div class="container">
        <div class="row">
            <?php foreach ($arResult['ITEMS'] as $arItem) {?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="market-items wow fadeIn animated" data-wow-delay="100ms" data-wow-duration="1500ms">
                        <div class="card border-0 rounded-0">
                            <div class="card-img position-relative">
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                    <div class="img-overlay style-1 activeColor d-flex align-items-center justify-content-center">
                                        <i class="fa fa-link" aria-hidden="true"></i>
                                    </div>
                                    <img class="card-img-top rounded-0 img-fluid" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                                </a>
                                <?php if ($arItem['PROPERTIES']['FLATICON']['VALUE']): ?>
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                        <div class="shape"></div>
                                        <div class="categories">
                                                <i class="flaticon-<?=$arItem['PROPERTIES']['FLATICON']['VALUE']?>"></i>
                                        </div>
                                    </a>
                                <?php endif;?>
                            </div>
                            <div class="card-body pl-0 pr-0">
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                    <h5 class="card-title mb-1"><?=$arItem['NAME']?></h5>
                                </a>
                                <p class="card-text"><?=$arItem['PREVIEW_TEXT']?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- End Market Section -->
