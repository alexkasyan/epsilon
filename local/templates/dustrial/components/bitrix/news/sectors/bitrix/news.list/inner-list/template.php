<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="market-list-group">
    <ul>
        <?php foreach ($arResult['ITEMS'] as $arItem){?>
            <?php
            if ($APPLICATION->GetCurDir() == $arItem['DETAIL_PAGE_URL']) {
                $class = 'active';
            }
            ?>
            <li><a class="<?=$class?>" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></li>
        <?php }?>
    </ul>
</div>
