<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="col-md-12 col-lg-12 col-xl-12 order-0 order-lg-1">
    <div class="market-content-page">
        <div class="block">
            <div class="text-img-block">
                <img class="img-fluid" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="">
                <h3 class="services-right-title"><?=$arResult['NAME']?></h3>
                <div class="detail-text">
                    <?=$arResult['DETAIL_TEXT']?>
                </div>
            </div>
                <?if ($arResult['PROPERTIES']['PHOTOS']['VALUE']):?>
                    <div class="gallery" style="margin-top: 40px;">
                            <h4><p><b>Фото</b></p></h4>
                            <hr>
                            <br>
                            <div class="row mb-4">
                                <?php foreach ($arResult['PROPERTIES']['PHOTOS']['VALUE'] as $value) {?>
                                    <?php $thumb = CFile::ResizeImageGet($value, ['width' => 200, 'height' => 200]);?>
                                    <div class="col-3 col-lg-3">

                                        <figure>
                                            <a class="d-block mb-4" data-fancybox="images" href="<?=CFile::GetPath($value)?>">
                                                <img class="img-fluid" src="<?=$thumb['src']?>">
                                            </a>
                                        </figure>
                                    </div>
                                <?php }?>
                            </div>
                    </div>
                <?php endif;?>

            <?if ($arResult['PROPERTIES']['VIDEOS']['VALUE']):?>
                <div class="gallery" style="margin-top: 40px;">
                    <h4><p><b>Видео</b></p></h4>
                    <hr>
                    <br>
                    <div class="row mb-4">
                        <?php foreach ($arResult['PROPERTIES']['VIDEOS']['VALUE'] as $value) {?>
                            <div class="col-3 col-lg-3">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:player",
                                    "",
                                    Array(
                                        "PLAYER_TYPE" => "auto",
                                        "USE_PLAYLIST" => "N",
                                        "PATH" => $value['path'],
                                        "WIDTH" => $value['width'],
                                        "HEIGHT" => $value['height'],
                                        "FULLSCREEN" => "Y",
                                        "SKIN_PATH" => "/bitrix/components/bitrix/player/mediaplayer/skins",
                                        "SKIN" => "bitrix.swf",
                                        "CONTROLBAR" => "bottom",
                                        "WMODE" => "transparent",
                                        "HIDE_MENU" => "N",
                                        "SHOW_CONTROLS" => "Y",
                                        "SHOW_STOP" => "N",
                                        "SHOW_DIGITS" => "Y",
                                        "CONTROLS_BGCOLOR" => "FFFFFF",
                                        "CONTROLS_COLOR" => "000000",
                                        "CONTROLS_OVER_COLOR" => "000000",
                                        "SCREEN_COLOR" => "000000",
                                        "AUTOSTART" => "N",
                                        "REPEAT" => "N",
                                        "VOLUME" => "90",
                                        "DISPLAY_CLICK" => "play",
                                        "MUTE" => "N",
                                        "HIGH_QUALITY" => "Y",
                                        "ADVANCED_MODE_SETTINGS" => "N",
                                        "BUFFER_LENGTH" => "10",
                                        "DOWNLOAD_LINK" => $value['path'],
                                        "DOWNLOAD_LINK_TARGET" => "_target"
                                    ));?>
                                    <p class="download-video"><a href="<?=$value['path']?>" target="_blank">скачать</a></p>
                            </div>
                        <?php }?>
                    </div>
                </div>
            <?php endif;?>
            <?//$APPLICATION->IncludeFile(SITE_DIR."include/service_icons_block.php");?>

    </div>
</div>
