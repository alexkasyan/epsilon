<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- Market Section -->
<div class="section-space">
    <div class="container">
        <div class="row">
            <?php foreach ($arResult['ITEMS'] as $arItem) {?>
                <div class="col-12 col-md-6 col-lg-4 jobs-block">
                    <div class=" wow fadeIn animated" data-wow-delay="100ms" data-wow-duration="1500ms">
                        <div class="card border-0 rounded-0">
                            <a class="job-card"  href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                <div class="job-card__text">
                                    <div class="job-card__title"><?=$arItem['NAME']?></div>
                                    <div class="job-card__desc"><?=$arItem['PREVIEW_TEXT']?></div>
                                </div>

                                <div class="job-card__more">
                                    <div class="job-card__more__wrap">
                                        <span><?=GetMessage('TO_GALLERY')?></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- End Market Section -->
