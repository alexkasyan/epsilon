<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- Latest Projects -->
<div class="project-section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if ($arResult['sectNames']) { ?>
                    <!-- Letest Project Btn -->
                    <div class="inner-mixitup-menus text-center">
                        <button class="btn btn-primary filter-btn mb-2 mb-md-0" type="button" data-mixitup-control data-filter="all"><?=GetMessage('FILTER_ALL')?></button>
                        <?php foreach ($arResult['sectNames'] as $sectName) {?>
                            <button class="btn btn-primary filter-btn mb-2 mb-md-0" type="button" data-mixitup-control data-filter=".<?=$sectName['CODE']?>"><?=$sectName['NAME']?></button>
                        <?php } ?>
                    </div><!-- End Project Btn -->
                <?php } ?>

                <!-- Letest Project content -->
                <div id="mixitup-projects" class="row">
                    <?php foreach ($arResult['ITEMS'] as $arItem) {?>
                        <div class="mix <?=$arResult['sectNames'][$arItem['IBLOCK_SECTION_ID']]['CODE']?> col-12 col-md-6 col-lg-4">
                            <div class="card border-0 rounded-0">
                                <div class="card-img position-relative">
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                        <div class="img-overlay activeColor d-flex align-items-center justify-content-center text-center">
                                            <div class="col-md-10">
                                                <h5 class="text-light"><?=$arItem['NAME']?></h5>
                                                <p class="text-light"><?=$arItem['PREVIEW_TEXT']?></p>
                                            </div>
                                        </div>
                                        <img class="card-img-top rounded-0 img-fluid" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
                                    </a>
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                        <div class="shape"></div>
                                        <div class="categories">
                                            <img class="img-fluid" src="<?=SITE_TEMPLATE_PATH?>/img/big-arrow.png" alt="cat img">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Letest Projects -->
