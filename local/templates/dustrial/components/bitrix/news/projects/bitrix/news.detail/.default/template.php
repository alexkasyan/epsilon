<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
    <?php if ($arResult['DETAIL_PICTURE']) {?>
        <div class="col-md-12 mb-4">
            <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="img-fluid" alt="<?=$arResult['NAME']?>">
        </div>
    <?php }?>
    <div class="col-md-6 col-lg-5 col-xl-4">
        <div class="single_project_widgets">
            <ul class="list-group">
                <?php if ($arResult['section']) {?>
                    <li class="list-group-item border-0 bg-transparent"><span class="font-weight-bold"><?=GetMessage('PROJECT_CATEGORY')?> :</span> <?=$arResult['section']['NAME']?></li>
                <?php }?>
                <?php if ($arResult['PROPERTIES']['PROJECT_CLIENT']['VALUE']) {?>
                    <li class="list-group-item border-0 bg-transparent"><span class="font-weight-bold"><?=GetMessage('PROJECT_CLIENT')?> :</span> <?=$arResult['PROPERTIES']['PROJECT_CLIENT']['VALUE']?></li>
                <?php }?>
                <?php if ($arResult['PROPERTIES']['PROJECT_DATE']['VALUE']) {?>
                    <li class="list-group-item border-0 bg-transparent"><span class="font-weight-bold"><?=GetMessage('PROJECT_DATE')?> :</span> <?=$arResult['PROPERTIES']['PROJECT_DATE']['VALUE']?></li>
                <?php }?>
                <?php if ($arResult['PROPERTIES']['PROJECT_LINK']['VALUE']) {?>
                    <li class="list-group-item border-0 bg-transparent"><span class="font-weight-bold"><?=GetMessage('PROJECT_LINK')?> :</span> <a target="_blank" href="//<?=$arResult['PROPERTIES']['PROJECT_LINK']['VALUE']?>"><?=$arResult['PROPERTIES']['PROJECT_LINK']['VALUE']?></a></li>
                <?php }?>
            </ul>
        </div>
    </div>
    <div class="col-md-6 col-lg-7">
        <div class="project-info">
            <h1><?=$arResult['NAME']?></h1>
            <div class="project-info-text">
                <?=$arResult['DETAIL_TEXT']?>
            </div>
        </div>
    </div>
</div>
