<?php
/**
 * @var array $arResult
 */
if ($arResult['IBLOCK_SECTION_ID']) {

        $dbSection = CIBlockSection::GetList(
            [],
            [
                'ID' => $arResult['IBLOCK_SECTION_ID']
            ],
            false,
            [
                'ID',
                'NAME'
            ]
        );

        while ($section = $dbSection->GetNext()) {
            $arResult['section'] = [
                'NAME' => $section['NAME'],
                'URL' => $section['SECTION_PAGE_URL']
            ];
        }
}