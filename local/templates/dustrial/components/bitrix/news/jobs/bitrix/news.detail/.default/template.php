<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="col-md-12 col-lg-7 col-xl-8 order-0 order-lg-1">
    <div class="market-content-page">
        <div class="block">
            <div class="text-img-block">
                <img class="img-fluid" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="">
                <h3 class="services-right-title"><?=$arResult['NAME']?></h3>
                <div class="detail-text">
                    <?=$arResult['DETAIL_TEXT']?>
                </div>
            </div>
            <?//$APPLICATION->IncludeFile(SITE_DIR."include/service_icons_block.php");?>

            <div class="vacancy-apply">
                <hr>
                <br>
                <?$APPLICATION->IncludeFile(SITE_DIR."include/job_apply.php");?>
            </div>
            <div id="brochures-block">
                <div class="list-group mt-3 mb-3 mt-lg-5 mb-lg-5 d-flex ">
                    <a class="list-group-item list-group-item-action download-brochures d-flex justify-content-between" target="_blank" href="/upload/job-apply.docx">
                        <?=GetMessage('DOWNLOAD_DOC')?>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/download-icon.png" class="download-icon" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
