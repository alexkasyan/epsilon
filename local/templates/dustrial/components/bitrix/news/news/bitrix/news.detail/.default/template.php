<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- blog single page -->

            <div class="col-md-12 col-lg-8">
                <div class="block row blog-details">
                    <div class="col-md-12">
                        <img class="img-fluid" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" >
                        <h3 class="blog-title mt-4"><?=$arResult['NAME']?></h3>
                        <div class="blog-mata">
                            <ul>
                                <li class="d-inline-block align-items-center">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <a><?=$arResult['DISPLAY_ACTIVE_FROM']?></a>
                                </li>
                            </ul>
                        </div>
                        <div class="blog-content">
                            <?=$arResult['DETAIL_TEXT']?>
                        </div>
                    </div>
                </div>
            </div>

<!-- End blog single page -->
