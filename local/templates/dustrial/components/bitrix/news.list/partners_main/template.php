<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (count($arResult["ITEMS"]) > 0) { ?>
    <div class="row">
        <div class="col-md-12">
            <div id="client-carousel-items" class="owl-carousel">
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>

                            <div class="client-single-items" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                <?php
                                if ($arItem['PROPERTIES']['LINK']['VALUE']) {
                                ?>
                                    <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">
                                        <img class="img-fluid sponsored-img" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
                                    </a>
                                <?php } else {?>
                                    <img class="img-fluid sponsored-img" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
                                <?php } ?>
                            </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php } ?>