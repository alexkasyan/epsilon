<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?php if (count($arResult['ITEMS']) > 0) { ?>
        <div class="row d-flex justify-content-center">
            <div class="col-md-8 col-lg-6">
                <div class="section-title-three text-center">
                    <h1><?=GetMessage('SECTORS_MAIN')?></h1>
                    <p><?=$arResult['DESCRIPTION']?></p>
                </div>
            </div>
        </div>

    <div class="row">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
                <div class="col-12 col-md-6 col-lg-4 market-area">
                    <div class="market-items">
                        <!-- Market Card
                        ===================== -->
                        <div class="card border-0 rounded-0  market-style-3">
                            <div class="card-img overflowhidden position-relative">

                                <!-- Create Your Post Link here
                                ================================= -->
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                    <div class="img-overlay activeColor d-flex align-items-center justify-content-center">
                                        <i class="fa fa-link" aria-hidden="true"></i>
                                    </div><!-- End Card Image Overlay Area -->
                                    <img class="card-img overflowhidden-top rounded-0 img-fluid" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
                                </a><!-- End Image Link Area -->

                                <?php $icon = $arItem['PROPERTIES']['FLATICON']['VALUE'] ? $arItem['PROPERTIES']['FLATICON']['VALUE'] : 'right-arrow'; ?>
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                    <div class="shape-style-2"></div><!-- Card Shape Style -->
                                    <div class="categories">
                                        <i class="flaticon-<?=$icon?>"></i>
                                    </div><!-- End Categories Area -->
                                </a>

                            </div>
                            <div class="card-body pl-0 pr-0">
                                <!-- Create Your Post Link here
                                ================================ -->
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                    <h5 class="card-title mb-1"><?=$arItem['NAME']?></h5>
                                </a>
                                <p class="card-text"><?=$arItem['PREVIEW_TEXT']?></p>
                            </div>
                        </div><!-- End Card -->

                    </div><!-- End block -->
                </div><!-- End Col -->
        <? endforeach;?>
    </div>
<?php }?>