<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (count($arResult['ITEMS']) > 0) { ?>
    <div class="row d-flex justify-content-center">
        <div class="col-md-8 col-lg-6">
            <div class="section-title-three text-center">
                <h1><?=GetMessage('OUR_EXPERTS')?></h1>
                <p><?=$arResult['DESCRIPTION']?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="team-item col-12 col-md-6 col-lg-3" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="card rounded-0 border-0">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" style="display: block;">
                        <div class="team-thumb position-relative">
                            <img class="card-img-top" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
                        </div>
                    </a>
                    <div class="card-body">
                        <h5 class="card-title mb-0"><?=$arItem['NAME']?></h5>
                        <p class="card-text"><?=$arItem['PREVIEW_TEXT']?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ;?>
    </div>

<?php }?>