<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (count($arResult['ITEMS']) > 0) { ?>
    <div class="row d-flex justify-content-center">
        <div class="col-md-8 col-lg-6">
            <div class="section-title-three text-center">
                <h1 class="text-light"><?=GetMessage('OUR_PROJECTS')?></h1>
                <p class="text-light"><?=$arResult['DESCRIPTION']?></p>
            </div>
        </div>
    </div>

    <?php if ($arResult['sectNames']) { ?>
        <!-- Letest Project Btn -->
        <div class="h3-mixitup-menus">
            <button class="btn btn-primary" type="button" data-mixitup-control data-filter="all"><?=GetMessage('FILTER_ALL')?></button>
            <?php foreach ($arResult['sectNames'] as $sectName) {?>
                <button class="btn btn-primary" type="button" data-mixitup-control data-filter=".<?=$sectName['CODE']?>"><?=$sectName['NAME']?></button>
            <?php } ?>
        </div><!-- End Project Btn -->
    <?php } ?>

    <!-- Letest Project content -->
    <div id="mixitup-projects" class="row">
        <?php foreach ($arResult['ITEMS'] as $ITEM) {?>
            <div class="mix <?=$arResult['sectNames'][$ITEM['IBLOCK_SECTION_ID']]['CODE']?> col-12 col-md-6 col-lg-4">
                <div class="card border-0 rounded-0">
                    <div class="card-img position-relative">

                        <!-- Create Your Post Link here
                        ================================= -->
                        <a href="<?=$ITEM['DETAIL_PAGE_URL']?>">
                            <div class="img-overlay style-1 activeColor d-flex align-items-center justify-content-center text-center">
                                <div class="col-md-10">
                                    <h5 class="text-light"><?=$ITEM['NAME']?></h5>
                                    <p class="text-light"><?=$ITEM['PREVIEW_TEXT']?></p>
                                </div>
                            </div><!-- End Card Image Overlay Area -->
                            <img class="card-img-top rounded-0 img-fluid" src="<?=$ITEM['PREVIEW_PICTURE']['SRC']?>" alt="<?=$ITEM['NAME']?>">
                        </a><!-- End Image Link Area -->

                        <!-- You can create you tags link in heare
                        ================================================ -->
                        <a href="<?=$ITEM['DETAIL_PAGE_URL']?>">
                            <div class="shape"></div><!-- Card Shape Style -->
                            <div class="categories">
                                <img class="img-fluid" src="<?=SITE_TEMPLATE_PATH?>/img/big-arrow.png" alt="cat img">
                            </div><!-- End Categories Area -->
                        </a>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
<?php }?>