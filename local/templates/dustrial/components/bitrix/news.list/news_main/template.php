<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (count($arResult['ITEMS']) > 0) { ?>
<div class="row d-flex justify-content-center">
                <div class="col-md-8 col-lg-6">
                    <div class="section-title-three text-center">
                        <h1><?=GetMessage('LATEST_NEWS')?></h1>
                        <p><?=$arResult['DESCRIPTION']?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="block">

    <div id="blog-list-3" class="owl-carousel">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
                <div class="h2-blog-single-item">
                    <?php if ($arItem['PREVIEW_PICTURE']) {?>
                        <div class="blog-img">
                            <img class="img-fluid" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
                            <?php if ($arResult['sectNames'] && $arResult['sectNames'][$arItem['IBLOCK_SECTION_ID']]) {?>
                                <div class="overlay-btn">
                                    <a href="<?=$arResult['sectNames'][$arItem['IBLOCK_SECTION_ID']]['URL']?>" class="btn btn-primary"><?=$arResult['sectNames'][$arItem['IBLOCK_SECTION_ID']]['NAME']?></a>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="card rounded-0">
                        <div class="card-body">
                            <div class="article-content">
                                <div class="entry-meta">
                                    <div class="month"><?=$arItem['DISPLAY_ACTIVE_FROM']?></div>
                                </div>
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="entry-title"><?=$arItem['NAME']?></a>
                                <p class="entry-content"><?=$arItem['PREVIEW_TEXT']?></p>
                            </div>
                            <div class="entry-meta-footer">
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="read-more"><?=GetMessage('READ_MORE')?> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
        <?php endforeach; ;?>
    </div>

                    </div>
                </div>
            </div>
<?php }?>