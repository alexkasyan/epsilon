<?php
/**
 * @var array $arResult
 */
if (count($arResult['ITEMS']) > 0 ) {
    $sections = [];
    foreach ($arResult['ITEMS'] as $arItem) {
        $sections[] = $arItem['IBLOCK_SECTION_ID'];
    }

    if ($sections) {
        $dbSections = CIBlockSection::GetList(
            [],
            [
                'ID' => $sections
            ],
            false,
            [
                'ID',
                'NAME',
                'SECTION_PAGE_URL'
            ]
        );

        while ($section = $dbSections->GetNext()) {
            $arResult['sectNames'][$section['ID']] = [
                'NAME' => $section['NAME'],
                'URL' => $section['SECTION_PAGE_URL']
            ];
        }
    }
}