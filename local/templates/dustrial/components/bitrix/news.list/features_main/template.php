<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (count($arResult["ITEMS"]) > 0) { ?>
    <div class="row d-flex justify-content-center">
        <div class="col-md-8 col-lg-6">
            <div class="section-title-three text-center">
                <h1><?=GetMessage('FEATURES_MAIN')?></h1>
                <p><?=$arResult['DESCRIPTION']?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="col-md-4">
                <div class="block position-relative mb-3 mb-md-0">
                    <img class="img-fluid" width="363px" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?echo $arItem["NAME"]?>">
                    <div class="featured-overlay position-absolute">
                        <h4 class="featured-title text-light mb-2 mb-lg-4">
                            <?echo $arItem["NAME"]?>
                        </h4>
                        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="btn btn-primary"><?=GetMessage('READ_MORE')?></a>
                    </div>
                </div>
            </div>
        <?endforeach;?>
    </div>
<?php } ?>