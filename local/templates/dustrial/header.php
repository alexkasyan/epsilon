<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
IncludeTemplateLangFile(__FILE__);
?>
    <html>
    <head>
        <!--- Basic Page Needs  -->
        <meta charset="utf-8">
        <title><?=$APPLICATION->ShowTitle()?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Specific Meta  -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=cyrillic" rel="stylesheet">
        <!-- Load Main Stylesheet File -->
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/style.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/responsive.css">

        <!-- Load Modernizr File -->
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/modernizr.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <? $APPLICATION->ShowHead(); ?>
        <title><? $APPLICATION->ShowTitle() ?></title>
    </head>

<body>

<? $APPLICATION->ShowPanel() ?>

    <!-- Loading Transition -->
    <!--<div id="loader-wrapper">
        <div id="loader"></div>
    </div>-->

    <!-- H3 header top -->
    <header class="h3-header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="h1-social-media">
                        <?$APPLICATION->IncludeFile(SITE_DIR."include/header_social.php");?>

                    </div>
                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                    <div class="h1-single-top-block text-center">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <strong><?=GetMessage('CALL')?>:</strong>
                        <?$APPLICATION->IncludeFile(SITE_DIR."include/header_tel.php");?>

                    </div>
                    <div class="h1-single-top-block text-center">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <strong><?=GetMessage('DAYS')?>:</strong>
                        <a> <?$APPLICATION->IncludeFile(SITE_DIR."include/header_time.php");?> / <?=GetMessage('CLOSED_INFO')?></a>
                    </div>
                    <div class="h1-single-top-block text-center">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <strong>Email:</strong>
                        <?$APPLICATION->IncludeFile(SITE_DIR."include/header_mail.php");?>
                    </div>
                </div>

            </div>
        </div>
    </header>
    <!-- End H3 header top -->
<?
if ($APPLICATION->GetCurPage(false) == SITE_DIR) {
    $isMain = true;
}

?>
    <!-- H1 Navigation -->
    <div id="<? if ($isMain) {
        echo 'h3-main-nav';
    } else {
        echo 'main-nav';
    } ?>"
         class="<? if ($isMain) {
             echo "h3-navigation-area";
         } else {
             echo "h1-navigation-area";
         } ?>">
        <nav class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="logo">

                        <?$APPLICATION->IncludeFile(SITE_DIR."/include/logo.php");?>
                    </div>


                    <div class="main-nav-area">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "header",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "top",
                                "USE_EXT" => "N"
                            )
                        );?>

                        <div class="nav-serch-area">
                            <div id="inline-popups" class="form-inline my-2 my-lg-0">
                                <a class="header-search" href="#test-popup" data-effect="mfp-zoom-in">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                                <?if(!$isMain && false){?>
                                    <a href="javascript:;" class="get_quote btn text-light my-2 my-sm-0">Get A Quote</a>
                                <?}?>
                                <div id="test-popup" class="white-popup mfp-with-anim mfp-hide">
                                    <form method="get" action="<?SITE_DIR?>/search/">
                                        <div class="form-group">
                                            <label for="Search"><?=GetMessage('SEARCH')?></label>
                                            <input type="text" class="form-control" id="Search" name="q"
                                                   placeholder="<?=GetMessage('SEARCH_KEY')?>">
                                        </div>
                                        <button type="submit" class="btn btn-primary"><?=GetMessage('SEARCH_SUB')?></button>
                                    </form>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!-- End Navigation -->

<? if (!$isMain && !defined('ERROR_404')) { ?>
    <div class="page_title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="page_tittle activeColor text-center text-uppercase"><? $APPLICATION->ShowTitle(false); ?></h5>

                    <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", Array(
                        "START_FROM" => "0",
                        // Номер пункта, начиная с которого будет построена навигационная цепочка
                        "PATH" => "",
                        // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                        "SITE_ID" => "s1",
                        // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                    ),
                        false
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <? if (!CSite::InDir(SITE_DIR . 'projects/')){ ?>
    <!--<div class="service-details-pages">
        <div class="container">
            <div class="row">-->
                <?}?>
<? } ?>