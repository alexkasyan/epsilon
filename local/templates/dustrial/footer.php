<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if(!$isMain && !CSite::InDir(SITE_DIR . 'projects/')) { ?>

            <!--</div>
        </div>
    </div>-->
    <?
}?>

<?
if ($APPLICATION->GetCurPage(false) === SITE_DIR.'contacts/') {
    $APPLICATION->IncludeFile(SITE_DIR."/include/footer-map.php");
}
?>

<?
if ($APPLICATION->GetCurPage(false) === SITE_DIR.'faq/') {
    $APPLICATION->IncludeFile(SITE_DIR."/include/footer-faq.php");
}
?>

<!-- h3 Footer Section -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3 col-xl-2">
                <div class="block mb-4 mb-lg-0">
                    <?$APPLICATION->IncludeFile(SITE_DIR."/include/logo.php");?>

                </div>
            </div>

            <div class="col-md-6 col-lg-3 col-xl-3">
                <div class="block mb-4 mb-lg-0">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "footer",
                        Array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(""),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "top",
                            "USE_EXT" => "N"
                        )
                    );?>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 col-xl-3">

            </div>

            <div class="col-md-6 col-lg-3 col-xl-4">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.line",
                    "footer-news",
                    Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "300",
                        "CACHE_TYPE" => "A",
                        "DETAIL_URL" => "",
                        "FIELD_CODE" => array("ID","CODE","NAME","PREVIEW_TEXT","PREVIEW_PICTURE","DATE_ACTIVE_FROM","DATE_CREATE",""),
                        "IBLOCKS" => array(EpsIblockHelper::NEWS_IBLOCK_ID_RU),
                        "IBLOCK_TYPE" => "news",
                        "NEWS_COUNT" => "2",
                        "SORT_BY1" => "ID",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC"
                    )
                );?>
            </div>
        </div>
    </div>
</footer>
<!-- End h3 Footer -->

<!-- h3 CopyRight Section -->
<div class="copyright">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="block copyright-content mb-2 mb-md-0">
                    <p class="m-0 text-light">
                        <?$APPLICATION->IncludeFile(SITE_DIR."include/copyright.php");?>

                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <?$APPLICATION->IncludeFile(SITE_DIR."include/social_box.php");?>
            </div>
        </div>
    </div>
</div>
<!-- End h3 CopyRight Section -->


<!-- Load Plugins File -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-3.2.0.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/owl.carousel.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.counterup.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.barfiller.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/stellarnav.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/mixitup.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.waypoints.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/wow.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/app.js"></script>

</body>
</html>
