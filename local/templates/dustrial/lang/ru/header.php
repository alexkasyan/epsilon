<?
$MESS["HDR_GOTO_MAIN"] = "На главную страницу";
$MESS["HDR_ASK"] = "Задайте вопрос";
$MESS["CALL"] = "Тел.";
$MESS["DAYS"] = "Пн - Птн";
$MESS["CLOSED_INFO"] = "Выходные сб, вс";
$MESS["READ_MORE"] = "Подробнее";
$MESS["OUR_PROJECTS"] = "Наши <strong>Проекты</strong>";
$MESS["FILTER_ALL"] = "Все";
$MESS["LATEST_NEWS"] = "Наши <strong>Новости</strong>";
$MESS["SECTORS_MAIN"] = "Наши <strong>Отрасли</strong>";
$MESS["FEATURES_MAIN"] = "<strong>Преимущества</strong>";
$MESS["OUR_EXPERTS"] = "Наши <strong>Эксперты</strong>";
$MESS["PROJECT_CATEGORY"] = "Категория";
$MESS["PROJECT_CLIENT"] = "Заказчик";
$MESS["PROJECT_DATE"] = "Дата завершения";
$MESS["PROJECT_LINK"] = "Ссылка";
$MESS["CALL_US"] = "Позвоните нам";
$MESS["OR_FILL"] = "или заполните форму";
$MESS["HEADQ"] = "Штаб-квартира компании";
$MESS["SEARCH_KEY"] = "Введите запрос";
$MESS["SEARCH"] = "Поиск";
$MESS["SEARCH_SUB"] = "Найти";
$MESS["DOWNLOAD_DOC"] = "Скачать анкету";
$MESS["TO_GALLERY"] = "Галерея"
?>