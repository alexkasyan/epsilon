<?php

class EpsIblockHelper
{
    const NEWS_IBLOCK_ID_RU = 1;
    const PROJECTS_IBLOCK_ID_RU = 3;
    const SECTORS_IBLOCK_ID_RU = 4;
    const FEATURES_IBLOCK_ID_RU = 7;
    const PARTNERS_IBLOCK_ID_RU = 8;
    const TEAM_IBLOCK_ID_RU = 5;
    const JOBS_IBLOCK_ID_RU = 2;
    const MEDIA_IBLOCK_ID_RU = 10;
}