<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<? if (!empty($arResult["ERROR_MESSAGE"])) {
    foreach ($arResult["ERROR_MESSAGE"] as $v) {
        ShowError($v);
    }
}
if (strlen($arResult["OK_MESSAGE"]) > 0) {
    ?>
    <div class="mf-ok-text"><?= $arResult["OK_MESSAGE"] ?></div><?
}
?>

<form action="" method="POST">
    <div class="row">
        <?= bitrix_sessid_post() ?>

        <div class="col-md-6">
            <div class="form-group">
                <input type="text" class="form-control rounded-0 p-4" name="user_name"
                       value="<?= $arResult["AUTHOR_NAME"] ?>" placeholder="<?= GetMessage("MFT_NAME") ?>*" required="">
            </div>
            <div class="form-group">
                <input class="form-control rounded-0 p-4"
                       type="email" name="user_email" value="<?= $arResult["AUTHOR_EMAIL"] ?>"
                       placeholder="<?= GetMessage("MFT_EMAIL") ?>*"
                       required="">
            </div>
            <div class="form-group">
                <input type="text" class="form-control rounded-0 p-4"
                       name="custom[]" value="" placeholder="<?= GetMessage("MFT_PHONE") ?>"
                >
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group">
                <textarea class="form-control rounded-0 p-4 faq_text_area" rows="5"
                          required
                          placeholder="<?= GetMessage("MFT_MESSAGE") ?>*" name="MESSAGE"
                ></textarea>
            </div>
        </div>

        <div class="col-md-12 d-flex justify-content-center mt-3">
            <button type="submit" name="submit" value="y" class="btn btn-primary"><?= GetMessage("MFT_SUBMIT") ?></button>
        </div>


    </div>
</form>
