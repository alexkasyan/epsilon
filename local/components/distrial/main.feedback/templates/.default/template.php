<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>
<div class="contact-feedback-block">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>

<form action="" method="POST">
<?=bitrix_sessid_post()?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" class="form-control" name="user_name"
                       value="<?=$arResult["AUTHOR_NAME"]?>" placeholder="<?=GetMessage("MFT_NAME")?>">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" class="form-control" name="custom[]" placeholder="<?=GetMessage("MFT_COMPANY")?>">
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="email"  name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>"
                       class="form-control" placeholder="<?=GetMessage("MFT_EMAIL")?>">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="text"  class="form-control" name="custom[]" value="" placeholder="<?=GetMessage("MFT_PHONE")?>">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <textarea class="form-control" rows="10" placeholder="<?=GetMessage("MFT_MESSAGE")?>" name="MESSAGE"></textarea>
            </div>
        </div>
    </div>
    <input type="submit" name="submit" class="btn btn-primary" value="<?=GetMessage("MFT_SUBMIT")?>">
   <!-- <button type="submit" class="btn btn-primary">send a message</button>



	<!--<div class="mf-name">
		<div class="mf-text">
			<?=GetMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>">
	</div>
	<div class="mf-email">
		<div class="mf-text">
			<?=GetMessage("MFT_EMAIL")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input type="text" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>">
	</div>

	<div class="mf-message">
		<div class="mf-text">
			<?=GetMessage("MFT_MESSAGE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<textarea name="MESSAGE" rows="5" cols="40"><?=$arResult["MESSAGE"]?></textarea>
	</div>
	<?foreach($arParams["NEW_EXT_FIELDS"] as $i => $ext_field):?>		
		<div class="mf-name">
			<div class="mf-text">
				<?=$ext_field?>
			</div>
			<input type="text" name="custom[<?$i?>]" value="<?=$arResult["custom_$i"]?>">
		</div>
	<?endforeach;?>
	<?if($arParams["USE_CAPTCHA"] == "Y"):?>
	<div class="mf-captcha">
		<div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
		<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
		<div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
		<input type="text" name="captcha_word" size="30" maxlength="50" value="">
	</div>
	<?endif;?>

	<input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">-->
</form>
</div>